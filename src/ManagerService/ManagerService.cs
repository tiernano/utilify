using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace Utilify.Platform.Manager
{
    internal partial class ManagerService : ServiceBase
    {
        private const int MagicServiceTimeout = 28000; //just a bit less than the 30 secs for the Windows SCM

        private ManagerHost host;

        public ManagerService()
        {
            InitializeComponent();
            host = new ManagerHost();
            host.OnShutDown += new EventHandler<EventArgs>(host_OnShutDown);
        }

        void host_OnShutDown(object sender, EventArgs e)
        {
            this.Stop();
        }

        protected override void OnStart(string[] args)
        {
            Thread managerStarter = new Thread(new ThreadStart(host.StartService));
            managerStarter.Name = "Manager Starter";
            managerStarter.Start();
        }

        protected override void OnStop()
        {
            if (host != null)
            {
                Thread managerStopper = new Thread(new ThreadStart(host.StopService));
                managerStopper.Name = "Manager Stopper";
                managerStopper.Start();
                managerStopper.Join(MagicServiceTimeout);

#if !MONO
                int timeout = 0;
                //try to get some more time upto *another* timeout period
                while (managerStopper.IsAlive && timeout <= MagicServiceTimeout)
                {
                    this.RequestAdditionalTime(1000);
                    timeout += 1000;
                }
#endif
                (host as IDisposable).Dispose();
            }
        }
    }
}
