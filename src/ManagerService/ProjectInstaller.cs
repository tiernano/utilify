using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace Utilify.Platform.Manager
{
    [RunInstaller(true)]
    internal partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}