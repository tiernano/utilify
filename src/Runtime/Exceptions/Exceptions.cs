﻿using System;
using System.Security.Permissions;
using System.Runtime.Serialization;

namespace Utilify.Platform
{
    /// <summary>
    /// The exception that is thrown when an executor with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class ExecutorNotFoundException : PlatformException
    {
        private string executorId;

        /// <summary>
        /// Initializes a new instance of <see cref="ExecutorNotFoundException">ExecutorNotFoundException</see> with the 
        /// specified error message and executor Id.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="executorId">The Id of the executor that could not be found.</param>
        public ExecutorNotFoundException(string message, string executorId)
            : this(message)
        {
            this.executorId = executorId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ExecutorNotFoundException">ExecutorNotFoundException</see> class.
        /// </summary>
        public ExecutorNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ExecutorNotFoundException">ExecutorNotFoundException</see> class
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public ExecutorNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ExecutorNotFoundException">ExecutorNotFoundException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ExecutorNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="ExecutorNotFoundException">ExecutorNotFoundException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected ExecutorNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
                executorId = info.GetString("ExecutorId");
        }

        /// <summary>
        /// Gets the executor Id value that could not be found
        /// </summary>
        public string ExecutorId
        {
            get
            {
                return executorId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("ExecutorId", executorId);
            base.GetObjectData(info, context);
        }
    }

    /// <summary>
    /// The exception that is thrown when a user with the specified name is not found.
    /// </summary>
    [Serializable]
    public class UserNotFoundException : PlatformException
    {
        private string userName;

        /// <summary>
        /// Initializes a new instance of <see cref="UserNotFoundException">UserNotFoundException</see> with the 
        /// specified error message and user name.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="userName">The name of the user that could not be found.</param>
        public UserNotFoundException(string message, string userName)
            : this(message)
        {
            this.userName = userName;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="UserNotFoundException">UserNotFoundException</see> class.
        /// </summary>
        public UserNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="UserNotFoundException">UserNotFoundException</see> class
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public UserNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="UserNotFoundException">UserNotFoundException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public UserNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="UserNotFoundException">UserNotFoundException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected UserNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
                userName = info.GetString("userName");
        }

        /// <summary>
        /// Gets the user name that could not be found
        /// </summary>
        public string UserName
        {
            get
            {
                return userName;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("userName", userName);
            base.GetObjectData(info, context);
        }
    }

    /// <summary>
    /// The exception that is thrown when an group with the specified Id is not found.
    /// </summary>
    [Serializable]
    public class GroupNotFoundException : PlatformException
    {
        private int groupId;

        /// <summary>
        /// Initializes a new instance of <see cref="GroupNotFoundException">GroupNotFoundException</see> with the 
        /// specified error message and group Id.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="groupId">The Id of the group that could not be found.</param>
        public GroupNotFoundException(string message, int groupId)
            : this(message)
        {
            this.groupId = groupId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupNotFoundException">GroupNotFoundException</see> class.
        /// </summary>
        public GroupNotFoundException() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupNotFoundException">GroupNotFoundException</see> class
        /// with the specified error message.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        public GroupNotFoundException(string message) : base(message) { }
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupNotFoundException">GroupNotFoundException</see> class 
        /// with the specified error message and inner exception that references the cause of this exception.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public GroupNotFoundException(string message, Exception innerException) : base(message, innerException) { }
        /// <summary>
        /// Initializes a new instance of <see cref="GroupNotFoundException">GroupNotFoundException</see> with
        /// the serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected GroupNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (info != null)
                groupId = info.GetInt32("groupId");
        }

        /// <summary>
        /// Gets the group Id value that could not be found
        /// </summary>
        public int GroupId
        {
            get
            {
                return groupId;
            }
        }

        /// <summary>
        /// See <see cref="M:System.Exception.GetObjectData">Exception.GetObjectData</see>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            //need to override this since this class adds a new field. (and one of the classes in the heirarchy implements ISerializable
            if (info == null)
                throw new ArgumentNullException("info");

            info.AddValue("groupId", groupId);
            base.GetObjectData(info, context);
        }
    }

}
