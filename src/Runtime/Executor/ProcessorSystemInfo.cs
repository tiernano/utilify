using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ProcessorSystemInfo
    {

        private string name;
        private string description; // CPU string
        private string vendor;
        private Architecture architecture;
        private double speedMax; //(in  GHz)

        private ProcessorSystemInfo(){}

        /// <summary>
        /// Creates and instance of ProcessorSystemInfo
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="vendor"></param>
        /// <param name="architecture"></param>
        /// <param name="speedMax"></param>
        public ProcessorSystemInfo(string name, string description, string vendor, Architecture architecture, double speedMax)
            : this()
        {
            this.Name = name;
            this.Description = description;
            this.Vendor = vendor;
            this.Architecture = architecture;
            this.SpeedMax = speedMax;
        }

        /// <summary>
        /// Gets the Name of this processor
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each processor on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Processor name cannot be empty", "name");
                name = value;
            }
        }

        /// <summary>
        /// Gets the manufacturers description for the processor.
        /// </summary>
        //May allow this to be empty in the future
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Description
        {
            get { return description; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("description");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Processor description cannot be empty", "description");
                description = value;
            }
        }

        /// <summary>
        /// Gets the vendor name for this processor.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Vendor
        {
            get { return vendor; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("vendor");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Processor vendor name cannot be empty", "vendor");
                vendor = value;
            }
        }

        /// <summary>
        /// Gets the architecture type for this processor.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public Architecture Architecture
        {
            get { return architecture; }
            private set
            {
                architecture = value;
            }
        }

        /// <summary>
        /// Gets the maximum clock speed rating (in MHz) for this processor.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public double SpeedMax
        {
            get { return speedMax; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Maximum processor speed cannot be less than zero", "speedMax");
                speedMax = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} : {4} Ghz (max.)", name, description, vendor, architecture, speedMax);
        }
    }
}
