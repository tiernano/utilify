using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class MemoryPerformanceInfo
    {
        // If using performance counters we can get some info about memory speed too. but problems in windows with performance counters
        // and some machines may not have them running (espc. Memory, Processor, etc.)

        private long sizeTotalFree; //(in bytes)
        private long sizeUsageCurrent; //(in bytes)

        private MemoryPerformanceInfo() { }

        public MemoryPerformanceInfo(long sizeTotalFree, long sizeUsageCurrent) : this()
        {
            this.SizeTotalFree = sizeTotalFree;
            this.SizeUsageCurrent = sizeUsageCurrent;
        }

        /// <summary>
        /// Gets the current size of free memory in bytes.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeTotalFree
        {
            get { return sizeTotalFree; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size free cannot be less than zero", "sizeTotalFree");
                sizeTotalFree = value;
            }
        }

        /// <summary>
        /// Gets the current size of free memory in bytes.
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public long SizeUsageCurrent
        {
            get { return sizeUsageCurrent; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Size usage cannot be less than zero", "sizeUsageCurrent");
                sizeUsageCurrent = value;
            }
        }

        /// <summary>
        /// Creates the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} bytes Total Free {1} bytes Currently Used", sizeTotalFree, sizeUsageCurrent);
        }
    }
}
