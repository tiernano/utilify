using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform.Contract
{
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ProcessorLimitInfo
    {
        private string name; // OS's name/id for this CPU used for linking during the updates
        private double loadUsageLimit; //(in %)

        private ProcessorLimitInfo(){}

        /// <summary>
        /// Creates an instance of ProcessorLimitInfo.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="loadUsageLimit"></param>
        public ProcessorLimitInfo(string name, double loadUsageLimit) : this()
        {
            this.Name = name;
            this.LoadUsageLimit = loadUsageLimit;
        }

        /// <summary>
        /// Gets the Name of this processor
        /// </summary>
        /// <remarks>
        /// This field uniquely identifies each processor on a machine.
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string Name
        {
            get { return name; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("name");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Processor name cannot be empty", "name");
                name = value;
            }
        }

        /// <summary>
        /// Gets the limit for the load on this processor.
        /// </summary>
        /// <remarks>
        /// This is either a limit for the Total usage (hard to control?)
        /// OR a limit on how much processor load can be used by the executor itself (also hard to control?).
        /// </remarks>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public double LoadUsageLimit
        {
            get { return loadUsageLimit; }
            private set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Processor load usage limit cannot be less than zero", "loadUsageLimit");
                loadUsageLimit = value;
            }
        }

        /// <summary>
        /// Gets the string representation of this object.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}% Load Usage Limit", loadUsageLimit);
        }
    }
}
