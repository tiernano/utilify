For 64-bit support:

We need a .NET framework that runs on 64-bit OS-es.
Both Windows and Linux have 64-bit OS-es. And there is nothing much to do, apart from testing on those OS-es, since .NET,Java are both managed platforms, should not cause too many troubles to port to 64-bit.

- May need to test performance, and optimize for 64-bit and have a seperate version for those OS-es.
- May need to modify Executor-Infoprovider, since it uses kernel32, other p/Invokes.

- 64-bit hardware is of 2 types:
	- x64 (AMD64 / EM64T)
	- Itanium
	- Other : Motorola RISC, Sun SPARC, Alpha, ARM etc.

We can support only what the OS supports and for which there is a .NET CLR.

Mono has CLRs on many Linux OS-es with different architectures.
Windows has CLRs for x64, Itanium.

