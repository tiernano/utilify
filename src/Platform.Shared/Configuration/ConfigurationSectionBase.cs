﻿using System.Configuration;

namespace Utilify.Platform.Configuration
{
    /// <summary>
    /// The base class for a configuration section.
    /// </summary>
    public abstract class ConfigurationSectionBase : System.Configuration.ConfigurationSection
    {
        //property useful for schema intellisense
        //This property is required on the class or else the ConfigurationManager won't load the section saying:
        //the configuration section contains an unknown attribute 'xmlns'

        private const string XmlnsPropertyName = "xmlns";

        /// <summary>
        /// Gets the XML namespace.
        /// </summary>
        /// <value>The namespace value.</value>
        [ConfigurationProperty(XmlnsPropertyName, IsRequired = false)]
        public string Xmlns
        {
            get { return (string)base[XmlnsPropertyName]; }
        }
    }
}