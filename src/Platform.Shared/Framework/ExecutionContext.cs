using System;
using Utilify.Platform;

namespace Utilify.Framework
{
    /// <summary>
    /// Represents the current context in which a job executes. This includes its environment, working directory and other details.
    /// </summary>
    [Serializable]
    public class ExecutionContext
    {
        //key (int) = Thread Id, value (ExecutionContext) = instance of Utilify.Framework.ExecutionContext
        //private static Dictionary<int, ExecutionContext> contexts =
        //    new Dictionary<int, ExecutionContext>();

        private string jobId;
        private string workingDirectory;
        private StringAppender log;
        private StringAppender error;

        //prevent exposing a default-public ctor
        private ExecutionContext() {}

        private ExecutionContext(string jobId, string workingDirectory, StringAppender log, StringAppender error) : this()
        {
            this.JobId = jobId;
            this.WorkingDirectory = workingDirectory;
            this.Log = log;
            this.Error = error;
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The job id.</value>
        public string JobId
        {
            get { return jobId; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("jobId");
                if (value.Trim().Length == 0 || Helper.GetGuidSafe(value) == Guid.Empty)
                    throw new ArgumentException("Job id cannot be empty or in a format other than a GUID", "jobId");
                jobId = value;
            }
        }

        /// <summary>
        /// Gets the working directory.
        /// </summary>
        /// <value>The working directory.</value>
        public string WorkingDirectory
        {
            get { return workingDirectory; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("workingDirectory");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Working directory cannot be empty", "workingDirectory");
                if (!System.IO.Directory.Exists(value))
                    throw new System.IO.DirectoryNotFoundException("Directory not found : " + value);

                workingDirectory = value; 
            }
        }

        /// <summary>
        /// Gets the log that an executing job can write to.
        /// </summary>
        /// <value>The log.</value>
        public StringAppender Log
        {
            get { return log; }
            private set 
            {
                if (log == null)
                    log = new StringAppender();
                else
                    log = value;
            }
        }

        /// <summary>
        /// Gets the error log an executing job can write to.
        /// </summary>
        /// <value>The error.</value>
        public StringAppender Error
        {
            get { return error; }
            private set 
            {
                if (error == null)
                    error = new StringAppender();
                else
                    error = value;
            }
        }

        #region Factory Helpers

        //internal use only : had to make it public to avoid InternalsVisibleTo
        /// <summary>
        /// Creates a new ExecutionContext.
        /// This method is for internal use only.
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="log"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public static ExecutionContext Create(string jobId, string workingDirectory, StringAppender log, StringAppender error)
        {
            ExecutionContext context = new ExecutionContext(jobId, workingDirectory, log, error);
            //lock (contexts)
            //{
            //    contexts[CurrentId] = context;
            //}
            return context;
        }

        //public static ExecutionContext Current
        //{
        //    get
        //    {
        //        ExecutionContext current = null;
        //        lock (contexts)
        //        {
        //            int threadId = CurrentId;
        //            if (contexts.ContainsKey(threadId))
        //                current = contexts[threadId];
        //        }
        //        return current;
        //    }
        //}

        //private static int CurrentId
        //{
        //    get { return Thread.CurrentThread.ManagedThreadId; }
        //}

        #endregion

        //internal void Destroy()
        //{
        //    lock (contexts)
        //    {
        //        int id = CurrentId;
        //        if (contexts.ContainsKey(id))
        //            contexts.Remove(id);
        //    }
        //}
    }
}
