using System;
using System.Collections.Generic;
using System.Text;

namespace Utilify.Framework
{
    //ExecutionContext.Current - this style of context is patented by MS. So we avoid that.
    /// <summary>
    /// Specifies the contract for an executable job.
    /// This is the main interface developers using the Utilify Framework API must implement to have a job execute custom code.
    /// In addition to implementing this interface, the class should also be serializable, so that it can be serialized and transferred 
    /// over a network.
    /// </summary>
    public interface IExecutable
    {
        /// <summary>
        /// Executes the job with the specified context.
        /// </summary>
        /// <param name="context">The job context.</param>
        void Execute(ExecutionContext context);
    }

    /*
    public interface IExecutableWithContext
    {
        void Execute(ExecutionContext context);
    }
    */
    
}
