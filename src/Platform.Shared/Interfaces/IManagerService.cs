﻿using System.ServiceModel;

namespace Utilify.Platform
{
    /// <summary>
    /// Defines the interface for a service provided by the Manager.
    /// </summary>
#if !MONO
    [ServiceContract(Namespace = Namespaces.Manager)]
#endif
    public interface IManagerService
    {
        //service interfaces derive from this
        //the benefit from this is a bit more strong-typing/constraints in the ProxyFactory, 
        //where Generic-typed proxies are created

#if !MONO
        /// <summary>
        /// Checks if the service is running.
        /// </summary>
        [OperationContract]
#endif
        void CheckService();
    }
}
