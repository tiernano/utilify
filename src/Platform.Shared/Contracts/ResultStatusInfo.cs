using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    //todoDoc

    /// <summary>
    /// Represents the information about the status of a job execution result.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class ResultStatusInfo
    {
        private string resultId;
        private string jobId;
        private ResultInfo info;

        private ResultStatusInfo() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultStatusInfo"/> class.
        /// </summary>
        /// <param name="resultId">The result id.</param>
        /// <param name="jobId">The job id.</param>
        /// <param name="info">The info.</param>
        public ResultStatusInfo(string resultId, string jobId, ResultInfo info) : this()
        {
            this.ResultId = resultId;
            this.JobId = jobId;
            this.Info = info;
        }

        /// <summary>
        /// Gets the result id.
        /// </summary>
        /// <value>The result id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ResultId
        {
            get { return resultId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("resultId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Result id cannot be empty", "resultId");
                resultId = value;
            }
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The job id.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string JobId
        {
            get { return jobId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("jobId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Job id cannot be empty", "jobId");
                jobId = value;
            }
        }

        /// <summary>
        /// Gets the result information.
        /// </summary>
        /// <value>The info.</value>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ResultInfo Info
        {
            get { return info; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("info");
                info = value;
            }
        }
    }
}