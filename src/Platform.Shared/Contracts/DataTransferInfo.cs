﻿using System.IO;
using System;

#if !MONO
using System.ServiceModel;
#endif

namespace Utilify.Platform
{

#if !MONO
    /// <summary>
    /// Represents an object (for example, a file or directory) 
    /// that is transferred using the data transfer service on the Manager.
    /// </summary>
    [MessageContract]
#endif
    public class DataTransferInfo : IDisposable
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTransferInfo"/> class.
        /// </summary>
        public DataTransferInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTransferInfo"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public DataTransferInfo(string filePath)
        {
            if (filePath == null)
                throw new ArgumentNullException("filePath");

            FileStream fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            this.ObjectName = Path.GetFileName(filePath);
            this.Length = fs.Length;
            this.Data = new SequentialReadOnlyStream(fs);
        }

        private long length;

        /// <summary>
        /// Gets or sets the length of data to transfer.
        /// </summary>
        /// <value>The length.</value>
#if !MONO
        [MessageHeader(MustUnderstand = true)]
#endif
        public long Length
        {
            get { return length; }
            set { length = value; }
        }

        private bool createPath = false;
        /// <summary>
        /// Gets or sets a value indicating whether to create path.
        /// </summary>
        /// <value><c>true</c> if the path should be created; otherwise, <c>false</c>.</value>
#if !MONO
        [MessageHeader(MustUnderstand = true)]
#endif
        public bool CreatePath
        {
            get { return createPath; }
            set { createPath = value; }
        }

        private bool overwrite = false;
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="DataTransferInfo"/> is overwrite.
        /// </summary>
        /// <value><c>true</c> if overwrite; otherwise, <c>false</c>.</value>
#if !MONO
        [MessageHeader(MustUnderstand = true)]
#endif
        public bool Overwrite
        {
            get { return overwrite; }
            set { overwrite = value; }
        }

        private string objectName;

        /// <summary>
        /// Gets or sets the name of the object (file/directory).
        /// </summary>
        /// <value>The name of the object.</value>
#if !MONO
        [MessageHeader(MustUnderstand = true)]
#endif
        public string ObjectName
        {
            get { return objectName; }
            set { objectName = value; }
        }

        private Stream data;

        /// <summary>
        /// Gets or sets the data stream.
        /// </summary>
        /// <value>The data.</value>
#if !MONO
        [MessageBodyMember]
#endif
        public Stream Data
        {
            get { return data; }
            set { data = value; }
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // close stream when the contract instance is disposed. 
            //this ensures that stream is closed when file download is complete, 
            //since download procedure is handled by the client and the stream 
            //must be closed on server.
            if (data != null)
            {
                data.Dispose();
                data = null;
            }
        }

        #endregion
    }

#if !MONO
    /// <summary>
    /// Represents the information about an object (for example, a file or directory) 
    /// that is transferred using the data transfer service on the Manager.
    /// </summary>
    [MessageContract]
#endif
    public class DataObjectInfo
    {
        //Id of the binary data record in db. used by executors to get direct access to the file
        private string objectId;

        /// <summary>
        /// Gets or sets the object id.
        /// </summary>
        /// <value>The object id.</value>
#if !MONO
        [MessageHeader(MustUnderstand = true)]
#endif
        public string ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        private string objectName;

        /// <summary>
        /// Gets or sets the name of the object.
        /// </summary>
        /// <value>The name of the object.</value>
#if !MONO
        [MessageBodyMember]
#endif
        public string ObjectName
        {
            get { return objectName; }
            set { objectName = value; }
        }
    }
}
