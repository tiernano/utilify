using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents a job that is submitted for remote execution
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class JobSubmissionInfo
    {
        private string jobId; //used when this object is sent from the manager to the executor

        private string applicationId;
        private DependencyInfo[] dependencies;
        private byte[] jobInstance;
        private ResultInfo[] expectedResults;
        private JobType jobType;
        private ClientPlatform clientPlatform;

        /// <summary>
        /// Creates an instance of the JobSubmissionInfo class with the specified parameters
        /// </summary>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Platform.JobType">JobType</see> values</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty Or, <br/>
        /// - jobType is Unknown
        /// </exception>
        public JobSubmissionInfo(string applicationId, JobType jobType, byte[] jobInstance)
            : this(applicationId, jobType, jobInstance, null, null) { }

        /// <summary>
        /// Creates an instance of the JobSubmissionInfo class with the specified parameters
        /// </summary>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Platform.JobType">JobType</see> values</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <param name="dependencies">a list of dependencies for this job</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty
        /// </exception>
        public JobSubmissionInfo(string applicationId, JobType jobType, byte[] jobInstance, IEnumerable<DependencyInfo> dependencies)
            : this(applicationId, jobType, jobInstance, dependencies, null) { }

        /// <summary>
        /// Creates an instance of the JobSubmissionInfo class with the specified parameters
        /// </summary>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Platform.JobType">JobType</see> values</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <param name="expectedResults">a list of filenames of the results expected to be produced by the job</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty
        /// </exception>
        public JobSubmissionInfo(string applicationId, JobType jobType, byte[] jobInstance, IEnumerable<ResultInfo> expectedResults)
            : this(applicationId, jobType, jobInstance, null, expectedResults) { }

        /// <summary>
        /// Creates an instance of the JobSubmissionInfo class with the specified parameters
        /// </summary>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Platform.JobType">JobType</see> values</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <param name="dependencies">a list of dependencies for this job</param>
        /// <param name="expectedResults">a list of results expected to be produced by the job</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty
        /// </exception>
        public JobSubmissionInfo(string applicationId, JobType jobType, byte[] jobInstance, 
            IEnumerable<DependencyInfo> dependencies, IEnumerable<ResultInfo> expectedResults)
        {
            this.JobId = string.Empty; //to ensure it is not a null string

            this.ApplicationId = applicationId;
            this.JobType = jobType;
            this.clientPlatform = ClientPlatform.DotNet; // Jobs submitted using this client should ALWAYS send DotNet.
            this.JobInstance = jobInstance;

            this.dependencies = Helper.GetCopy<DependencyInfo>(dependencies);
            this.expectedResults = Helper.GetCopy<ResultInfo>(expectedResults);
        }

        // Note: In this constructor we need to set the ClientPlatform based on what was stored for the Job since it could have
        //       come from the Java client and we cant assume DotNet at this point.

        // Internal use only - to be used in the communication between the manager and the executor
        /// <summary>
        /// Creates an instance of the JobSubmissionInfo class with the specified parameters. 
        /// This constructor is for internal use only.
        /// </summary>
        /// <param name="jobId">id of the submitted job</param>
        /// <param name="applicationId">id of the associated application</param>
        /// <param name="jobType">type of the job which is one of the <see cref="Utilify.Platform.JobType">JobType</see> values</param>
        /// <param name="clientPlatform">client platform <see cref="Utilify.Platform.ClientPlatform">ClientPlatform</see> that was used to submit this Job</param>
        /// <param name="jobInstance">serialized instance of the job object</param>
        /// <param name="dependencies">a list of dependencies for this job</param>
        /// <param name="expectedResults">a list of results expected to be produced by the job</param>
        /// <exception cref="System.ArgumentNullException">applicationId or jobInstance is a null reference</exception>
        /// <exception cref="System.ArgumentException">
        /// - applicationId is empty Or, <br/>
        /// - jobInstance is empty
        /// </exception>
        public JobSubmissionInfo(string jobId, string applicationId, JobType jobType, ClientPlatform clientPlatform, byte[] jobInstance,
            IEnumerable<DependencyInfo> dependencies, IEnumerable<ResultInfo> expectedResults)
            : this(applicationId, jobType, jobInstance, dependencies, expectedResults)
        {
            //the JobId property setter is used in deserialization - but 
            //if this ctor is used directly the JobId should never be set to null/empty
            if (jobId == null)
                throw new ArgumentNullException("jobId");
            if (jobId.Trim().Length == 0)
                throw new ArgumentException("Job id cannot be empty", "jobId");

            this.JobId = jobId;
            this.ClientPlatform = clientPlatform;
        }

        /// <summary>
        /// Gets the job id
        /// </summary>
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        public string JobId
        {
            get { return jobId; }
            private set { jobId = value ?? string.Empty; }
        }

        /// <summary>
        /// Gets the id of the application associated with this job
        /// </summary>
        /// <exception cref="System.ArgumentNullException">applicationId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationId is empty</exception>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationId
        {
            get { return applicationId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application id cannot be empty", "applicationId");

                applicationId = value;
            }
        }

        /// <summary>
        /// Gets the type of the job
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public JobType JobType
        {
            get { return jobType; }
            private set 
            {
                jobType = value; 
            }
        }

        /// <summary>
        /// Identifies the platform the job was submitted from
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public ClientPlatform ClientPlatform
        {
            get { return clientPlatform; }
            private set
            {
                clientPlatform = value;
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        private DependencyInfo[] Dependencies
        {
            get { return dependencies; }
            set 
            {
                dependencies = Helper.GetCopy<DependencyInfo>(value);
            }
        }


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        private byte[] JobInstance
        {
            get { return jobInstance; }
            set 
            {
                //we are not doing a null check here, since this class may be passed around to components which don't need/want a job instance.
                //the checks are done where-ever we need the actual instance (Manager, Executor)
                jobInstance = value; 
            }
        }


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
#if !MONO
        [DataMember(IsRequired = false)]
#endif
        private ResultInfo[] ExpectedResults
        {
            get { return expectedResults; }
            set 
            {
                expectedResults = Helper.GetCopy<ResultInfo>(value);
            }
        }

        /// <summary>
        /// Gets the dependencies of the job
        /// </summary>
        /// <returns>an array of dependencies for the job</returns>
        public DependencyInfo[] GetDependencies()
        {
            return dependencies;
        }

        /// <summary>
        /// Gets the serialised instance of the job
        /// </summary>
        /// <returns>serialised instance of the job</returns>
        public byte[] GetJobInstance()
        {
            return jobInstance;
        }

        /// <summary>
        /// Gets the results expected to be created by the job once execution is complete
        /// </summary>
        /// <returns>an array of filenames that are expected to be produced by the job</returns>
        public ResultInfo[] GetExpectedResults()
        {
            return expectedResults;
        }
    }
}