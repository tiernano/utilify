using System;

#if !MONO
using System.Runtime.Serialization;
#endif

namespace Utilify.Platform
{
    /// <summary>
    /// Represents a query regarding the status of dependencies of an application or job.
    /// </summary>
    [Serializable]
#if !MONO
    [DataContract]
#endif
    public class DependencyQueryInfo
    {
        private string applicationOrJobId;
        private DependencyScope scope;

        private DependencyQueryInfo() { }

        /// <summary>
        /// Creates an instance of the dependency query object
        /// </summary>
        /// <param name="applicationOrJobId">The id of the application or job to query</param>
        /// <param name="scope">scope of the query (one of the enumeration values defined in <see cref="DependencyScope">DependencyScope</see></param>
        /// <exception cref="System.ArgumentNullException">applicationOrJobId is a null reference</exception>
        /// <exception cref="System.ArgumentException">applicationOrJobId is an empty string</exception>
        public DependencyQueryInfo(string applicationOrJobId, DependencyScope scope) : this()
        {
            this.ApplicationOrJobId = applicationOrJobId;
            this.Scope = scope;
        }

        /// <summary>
        /// Gets the application or job id that is being queried
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public string ApplicationOrJobId
        {
            get { return applicationOrJobId; }
            private set
            {
                if (value == null)
                    throw new ArgumentNullException("applicationOrJobId");
                if (value.Trim().Length == 0)
                    throw new ArgumentException("Application or job id cannot be empty", "applicationId");
                applicationOrJobId = value; 
            }
        }

        /// <summary>
        /// Gets the scope of the query
        /// </summary>
#if !MONO
        [DataMember(IsRequired = true)]
#endif
        public DependencyScope Scope
        {
            get { return scope; }
            private set { scope = value; }
        }

    }
}
