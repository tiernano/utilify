using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace Utilify.Framework
{
    /// <summary>
    /// A helper class to run a native executable on the local machine. 
    /// This class is for internal use only.
    /// </summary>
    public class LocalExecutionHelper
    {
        /// <summary>
        /// Provides the execution event handler.
        /// </summary>
        /// <param name="exec">The exec.</param>
        public static void ProvideExecutionEventHandler(NativeExecutable exec)
        {
            exec.ExecutionStarted += new EventHandler<ExecutionEventArgs>(exec_ExecutionStarted);
        }

        private static void exec_ExecutionStarted(object sender, ExecutionEventArgs e)
        {
            ExecutionArgs executionArgs = e.ExecutionArgs;

            // Call the executable with required arguments
            Process proc = new Process();
            proc.StartInfo.FileName = executionArgs.ExecutablePath;
            proc.StartInfo.Arguments = executionArgs.Arguments;
            proc.StartInfo.WorkingDirectory = executionArgs.WorkingDirectory;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;

            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.RedirectStandardOutput = true;

            proc.Start();
            proc.WaitForExit();

            String stdOutPath = Path.Combine(executionArgs.WorkingDirectory, executionArgs.StdOutFile);
            using (FileStream fs = File.Create(stdOutPath)) { }
            using (FileStream fs = File.OpenWrite(stdOutPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = proc.StandardOutput.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }

            String stdErrPath = Path.Combine(executionArgs.WorkingDirectory, executionArgs.StdErrFile);
            using (FileStream fs = File.Create(stdErrPath)) { }
            using (FileStream fs = File.OpenWrite(stdErrPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = proc.StandardError.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }
        }

        private void ExecuteLocally(ExecutionArgs executionArgs)
        {
        }
    }
}
