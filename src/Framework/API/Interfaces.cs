using Utilify.Platform;

namespace Utilify.Framework
{
    /// <summary>
    /// Specifies the contract for a job that depends on external input files.
    /// </summary>
    public interface IDependent
    {
        /// <summary>
        /// Gets the list of dependencies.
        /// </summary>
        /// <returns></returns>
        DependencyInfo[] GetDependencies();
    }

    /// <summary>
    /// Specifies the contract for a job that produces one or more result files.
    /// </summary>
    public interface IResults
    {
        /// <summary>
        /// Gets the list of expected results.
        /// </summary>
        /// <returns></returns>
        ResultInfo[] GetExpectedResults();
    }
}