package com.utilify.platform.executor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import com.utilify.framework.client.JobCompletionInfo;

public class JavaExecutor {

	public static final int DEFAULT_PORT = 9000;
	private Socket sock;
	private InputStream inputStream;
	private OutputStream outputStream;
	private ExecutionManager execManager;
	
    private Logger logger = Logger.getLogger(this.getClass().getName());
	
	public JavaExecutor() {
		this(DEFAULT_PORT);
	}

	public JavaExecutor(int port) {
		
		execManager = new ExecutionManager();
		
		// Connect back to the Executor
		boolean connected = false;
		int retry = 0;

		// Retry 3 times just incase we have some trouble. 
		// Really it should connect the first time
		// unless there is some issue.
		while(!connected && retry < 3) {
			try {
				logger.fine("Connecting...");
				sock = new Socket("127.0.0.1", port);
				inputStream = sock.getInputStream();
				outputStream = sock.getOutputStream();
				connected = true;
			} catch (UnknownHostException e) {
				//should never happen : since 127.0.0.1 is localhost
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			retry++;
			
			if (connected)
				break;
			
			try { Thread.sleep(1000); } catch (Exception e) {}
		}
		logger.fine("Connected? " + connected);
	}
	
	public static void main(String args[]){
		//Read any settings from command line passed 
	    // by the Executor when launching this process.
        int port = JavaExecutor.DEFAULT_PORT;
        if (args.length == 1){
        	try {
        		port = Integer.parseInt(args[0]);
        	} catch (NumberFormatException nfx) {}
        }
        
	    System.out.println("Starting JavaExecutor to connect back on port: " + port);
        JavaExecutor ex = new JavaExecutor(port);
	    ex.start();
	}

	void start() {
		try {
			String msgType;
			while(true) {
				// Read line or entire msg. Need to decide.
				msgType = MessageDataHelper.readNextLine(inputStream);
				logger.fine("Read message type: " + msgType);
				
				if (MessageType.isDefined(msgType)) {
					//we got and understood the message
					MessageDataHelper.sendAck(outputStream);

					if (msgType.equals(MessageType.ABORT_JOB.value())) {
						handleAbortJob();
					} else if (msgType.equals(MessageType.EXECUTE_JOB.value())) {
						handleExecuteJob(); 
					} else if (msgType.equals(MessageType.GET_COMPLETED_JOBS.value())) {
						handleGetCompletedJobs();
					} else if (msgType.equals(MessageType.SHUTDOWN.value())) {
						handleShutdown();
						break;
					}
				} else {
					MessageDataHelper.sendNack(outputStream);
				}
			}
		} catch (Exception e) {
			logger.fine("A Fatal Error Occurred: " + e.getMessage() + "\n" + e.getStackTrace().toString());
			e.printStackTrace();
		}
	}

	//------------ Command handlers
	
	private void handleShutdown() {
		//clean up here.
		try {
			inputStream.close();
			outputStream.close();
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		inputStream = null;
		outputStream = null;
		sock = null;
	}

	private void handleGetCompletedJobs() throws IOException {		

        logger.fine("Handling completed jobs");
        JobCompletionInfo[] completedJobs = getCompletedJobs();
        JobCompletionMessageData msgData = new JobCompletionMessageData(completedJobs);
        
        logger.fine(String.format("Got %d completed jobs", msgData.getJobs().length));
        logger.fine("Writing data back to Executor");
        MessageDataHelper.writeNextBytes(outputStream, msgData.toBytes());

        boolean gotAck = MessageDataHelper.receiveAck(inputStream);
        logger.fine("Received ACK? " + gotAck);
        //if (gotAck == true)
        //    delete the list of completed jobs that we just sent.
	}

	private void handleExecuteJob() 
		throws Exception {
		
		byte[] inData;
		logger.fine("Handling ExecuteJob...");
		// Read JobData
		inData = MessageDataHelper.readNextBytes(inputStream);
		ExecuteJobMessageData msgData = new ExecuteJobMessageData(inData);
		if (msgData.isValid()){
			MessageDataHelper.sendAck(outputStream);
			executeJob(msgData);
		} else {
			MessageDataHelper.sendNack(outputStream);
		}
	}
	
	private void handleAbortJob() throws IOException {
		byte[] inData;
		logger.fine("Handling AbortJob...");
		inData = MessageDataHelper.readNextBytes(inputStream);
		logger.fine("Creating AbortJobMessageData object...");
		AbortJobMessageData msgData = new AbortJobMessageData(inData);
		if (msgData.isValid()){
			MessageDataHelper.sendAck(outputStream);
			abortJob(msgData.getJobId());
		} else {
			MessageDataHelper.sendNack(outputStream);
		}
	}

	//--------------- Commands 
	
	private void abortJob(String jobId) {
		// Abort job here.
		logger.fine("Aborting Job " + jobId);
		execManager.abortJob(jobId);
	}

	private void executeJob(ExecuteJobMessageData msgData) throws Exception {
		
		// Execute job here
		logger.fine("Executing Job " + msgData.getJobId());
		execManager.executeJob(msgData.getJobId(),
				msgData.getApplicationId(),
				msgData.getJobDirectory(),
				msgData.getApplicationDirectory(),
				msgData.getJobType(),
				msgData.getInitialJobInstance());
	}

	private JobCompletionInfo[] getCompletedJobs() {
		return execManager.getCompletedJobs();
	}
}
