package com.utilify.platform.executor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.logging.Logger;

import com.utilify.framework.ErrorMessage;
import com.utilify.framework.ExecutionContext;
import com.utilify.framework.StringAppender;
import com.utilify.framework.client.Executable;

public class JvmExecutor {

	private ArrayList<ExecutionListener> listeners = new ArrayList<ExecutionListener>();
	
	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	public JvmExecutor(){}
	
	public void executeTask(JobExecutionInfo execInfo) {
		
        //create the ExecutionContext
        //this ExecutionContext thing seems to be patented by MS. Perhaps we could verify again.
        //So the way to do this would be to change the signature of the 'Execute' to :
        //  Execute(ExecutionContext context);
        ExecutionContext context = ExecutionContext.create(execInfo.getJobId(),
        		execInfo.getJobDirectory(), new StringAppender(), new StringAppender());

        Exception executionException = null;
        byte[] finalJobInstance = null;
        StringAppender executionLog = context.getLog();
        StringAppender executionError = context.getError();

        try{
            if (execInfo == null)
                throw new IllegalArgumentException("ExecutionInfo cannot be null");

            if (execInfo.getJobInstance() == null)
                throw new IllegalArgumentException("Job instance cannot be null");
            if (execInfo.getJobInstance().length == 0)
                throw new IllegalArgumentException("Job instance cannot be empty");
            if (execInfo.getJobDirectory() == null)
                throw new IllegalArgumentException("execInfo.JobDirectory");
            if (execInfo.getJobDirectory().trim().length() == 0)
                throw new IllegalArgumentException("Job directory cannot be empty");

            //before running anything - just set the final instance to be the same as the initial. So that we don't ever have a null instance.
            finalJobInstance = execInfo.getJobInstance();
            
            URLClassLoader cl = (URLClassLoader) this.getClass().getClassLoader();
            logger.fine("ClassLoader for this JvmExecutor: " + cl);
            for (URL url : cl.getURLs()) {
            	logger.fine("URL loaded: " + url.getPath());
            }
            
            //this.getClass().getClassLoader().loadClass("com.utilify.platform.test.AdditionJob");
            //System.out.println("Loaded class com.utilify.platform.test.AdditionJob");
            
            executionLog.appendLine("Working Directory: " + execInfo.getJobDirectory());

            // Deserialise the Job and cast to IExecutable
            executionLog.appendLine("Deserialising job...");
            Object deserialisedJob = deserialize(execInfo.getJobInstance());
            executionLog.appendLine("Successfully deserialised job? " + (deserialisedJob != null));

            executionLog.appendLine("Casting to Executable interface.");
            Executable exec = (Executable)deserialisedJob;
            executionLog.appendLine("Successfully cast job? " + (exec != null && exec instanceof Executable));
            
            // Execute Job
            exec.execute(context);

            executionLog.appendLine("Executed job. Serialising it again...");
            // Serialise the completed Job
            byte[] serialisedJob = serialize(exec);
            executionLog.appendLine("Serialized job. Returning final instance...");
            // Assign the final instance
            finalJobInstance = serialisedJob;
        } catch (Exception ex) {
        	StringWriter sw = new StringWriter();
        	PrintWriter pw = new PrintWriter(sw);
        	ex.printStackTrace(pw);
        	String fullErrorMessage = sw.toString();
        	pw.close();
            executionError.appendLine("Error executing job: " + fullErrorMessage);
            executionException = ex;
        }

        // Raise Completed Event.
        ExecutionCompleteEventArgs completeArgs = 
            new ExecutionCompleteEventArgs(
            execInfo.getJobId(),
            execInfo.getApplicationId(), 
            finalJobInstance, 
            executionLog, 
            executionError, 
            executionException);
        
        raiseCompletedEvent(completeArgs);
	}
	
	public void addListener(ExecutionListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
		synchronized (listeners) {
			listeners.add(listener);
		}
	}
	public void removeListener (ExecutionListener listener){
    	if (listener == null)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNull + ": listener");
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}
	
	private void raiseCompletedEvent(ExecutionCompleteEventArgs data){
		ArrayList<ExecutionListener> copy = new ArrayList<ExecutionListener>();
		synchronized (listeners) {
			copy.addAll(listeners);
		}
		for (ExecutionListener listener : copy) {
			try{
				if (listener != null) //just to be extra safe
					listener.executionComplete(this, data);
			}catch (Exception ex){
				System.out.println("Error raising event: " + ex.toString());
			}
		}
	}
	
    /**
     * Serializes the specified object and returns the serialized data.
     * @param obj - the object to serialize.
     * @return the binary data of the serialized object.
     * @throws IOException if there is an I/O error serializing the object.
     * @throws IllegalArgumentException if <code>obj</code> is a null reference.
     */
    private byte[] serialize(Object obj) throws IOException {
        // todoLater: abstract this out to use whatever serializer : to perhaps
		// conditionally compress / encrypt etc
        //for now we're just using a binary one
        ByteArrayOutputStream bos = new ByteArrayOutputStream() ;
        ObjectOutputStream out = null;
        byte[] instance = null;
        try {
        	out = new ObjectOutputStream(bos) ;
        	out.writeObject(obj);
        	instance = bos.toByteArray();
        } finally {
        	try{
        		bos.close();
        		if (out != null)
        			out.close();
        	}catch (IOException ix){
        		ix.printStackTrace();
        	}
        }
        
    	return instance;
    }
     
	/**
	 * Deserializes the specified data and returns the deserialized object.
	 * @param instance - the serialized data.
	 * @return the object instance represented by the specified data.
	 * @throws IOException if there is an I/O error deserializing the object.
	 * @throws IllegalArgumentException if <code>instance</code> is a null reference.
	 */ 
	private Object deserialize(byte[] instance) throws IOException, ClassNotFoundException {
				
    	if (instance == null || instance.length == 0)
    		throw new IllegalArgumentException(ErrorMessage.ValueCannotBeNullOrEmpty + ": instance");
    		
    	Object obj = null;
    	ByteArrayInputStream bis = new ByteArrayInputStream(instance);
    	ObjectInputStream in = null;
    	try {
    		in = new ObjectInputStream(bis);
    		obj = in.readObject();
//    	} catch (ClassNotFoundException e) {
    		//e.printStackTrace();
    		//logger.warning("Error deserializing object input stream: " + getFullStackTrace(e));
		} finally {
    		try{
        		bis.close();
        		if (in != null)
        			in.close();
        	}catch (IOException ix){
        		//ix.printStackTrace();
        		//logger.fine("Error closing object output stream: " + ix);
        	}    		
    	}
    	
    	return obj;
    }
}
