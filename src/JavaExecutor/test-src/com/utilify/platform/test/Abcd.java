package com.utilify.platform.test;


import java.io.Serializable;

public class Abcd implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String text;

    public Abcd(String text) 
    {
        this.text = text;
    }

    public String GetText()
    {
        return this.text;
    }
}