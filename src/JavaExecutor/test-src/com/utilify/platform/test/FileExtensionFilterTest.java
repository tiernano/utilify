package com.utilify.platform.test;

import java.io.File;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.utilify.platform.executor.FileExtensionFilter;

public class FileExtensionFilterTest {

	@Before
	public void setUp() throws Exception {
		
		File temp = new File("./temp");
		if (!temp.exists()){
			temp.mkdir();
		} else {
			File[] files = temp.listFiles();
			for (File f : files){
				f.delete();
			}
		}
		
		for (int i = 0; i < 12; i++){
			File f = new File(temp, "f" + i + ".n");
			f.createNewFile();
		}
		
		for (int i = 0; i < 23; i++){
			File f = new File(temp, "f" + i + ".ne");
			f.createNewFile();
		}

		for (int i = 0; i < 20; i++){
			File f = new File(temp, "f" + i + ".new");
			f.createNewFile();
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAccept(){
		File temp = new File("./temp");
		File[] files;
		
		FileExtensionFilter ff1 = new FileExtensionFilter(".n");
		files = temp.listFiles(ff1);
		Assert.assertEquals(12, files.length);
		
		FileExtensionFilter ff2 = new FileExtensionFilter(".ne");
		files = temp.listFiles(ff2);
		Assert.assertEquals(23, files.length);

		FileExtensionFilter ff3 = new FileExtensionFilter(".new");
		files = temp.listFiles(ff3);
		Assert.assertEquals(20, files.length);

	}
	
	
}
