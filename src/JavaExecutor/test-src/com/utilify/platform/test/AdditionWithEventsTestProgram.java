package com.utilify.platform.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.utilify.framework.ApplicationInitializationException;
import com.utilify.framework.ErrorEvent;
import com.utilify.framework.ErrorListener;
import com.utilify.framework.JobCompletedEvent;
import com.utilify.framework.JobCompletedListener;
import com.utilify.framework.JobStatusEvent;
import com.utilify.framework.JobStatusListener;
import com.utilify.framework.client.Application;
import com.utilify.framework.client.Job;

public class AdditionWithEventsTestProgram implements JobCompletedListener, JobStatusListener, ErrorListener {

    private static Logger logger = Logger.getLogger(AdditionWithEventsTestProgram.class.getName());

    public static void main(String[] args) throws IOException, 
    	ApplicationInitializationException, InterruptedException {

    	Logger rootLogger = Logger.getLogger("");
    	Handler[] handlers = rootLogger.getHandlers();
    	for (int i = 0; i < handlers.length; i++){
    		if (handlers[i] instanceof ConsoleHandler){
    			handlers[i].setLevel(Level.ALL);
    		}
    	}
    	
        logger.info("Started logging...");

        AdditionWithEventsTestProgram prog = new AdditionWithEventsTestProgram();
        prog.test();
        
        System.out.println("Press enter to exit...");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
    }

    private void test() throws ApplicationInitializationException, InterruptedException{
    	logger.fine("Starting client api test...");

        Application app = new Application("My Addition With Events Application");

        for (int i = 1; i < 10; i++) {
            AdditionJob job = new AdditionJob(i, i);
            app.addJob(job);
        }

        app.addErrorListener(this);
        app.addStatusListener(this);
        app.addJobCompletedListener(this);
        
        logger.info("Submitting Application");
        app.start();

        System.out.println("Waiting for results..." + this);
    }
    
	public void onError(ErrorEvent event) {
		System.out.println("I blew up: \n" +  event.getException().toString());
		if (event.getException().getCause() != null)
			System.out.print("Cause: ");
			event.getException().getCause().printStackTrace();
	}

	public void onJobCompleted(JobCompletedEvent event) {
		Job remoteJob = event.getJob();
        AdditionJob myJob = (AdditionJob)remoteJob.getCompletedInstance();
        String s = String.format("Job %s Completed in %d ms. %d + %d = %d",
                        remoteJob.getId(),
                        (event.getJob().getFinishTime().getTime() - remoteJob.getStartTime().getTime()),
                        myJob.getValueA(),
                        myJob.getValueB(),
                        myJob.getResult());
        System.out.println(s);
	}

	public void onJobStatusChanged(JobStatusEvent event) {
		System.out.println("********** Job status changed to " + event.getStatusInfo().getStatus());
	}
}
