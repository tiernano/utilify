package com.utilify.platform.test;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

public class Something {
	public static void main (String[] args) throws Exception{
		
		
		File appDir = new File("C:/Documents and Settings/Krishna/Application Data/Utilify/Applications/0f723857-af19-434f-8a4b-9a4a010639d2");
        File[] files = appDir.listFiles();
        ArrayList<URL> urls = new ArrayList<URL>();
        if (files != null){
        	for (File file : files){
        		if (file.getName().endsWith(".jar"))
        			urls.add(file.toURL());
        	}
        }
        
        //2. create a child classloader that will use those libs
        URLClassLoader urlCl = new URLClassLoader(urls.toArray(new URL[urls.size()]));

        URL[] urlsTemp = ((URLClassLoader)urlCl).getURLs();
        for (URL url : urlsTemp)
        	System.out.println("Url in cl : " + url);

        try{
        	Class c =  urlCl.loadClass("com.utilify.platform.executor.JvmExecutor");
        	System.out.println("Class found " + c.getCanonicalName());
        }catch (Exception ex){
        	ex.printStackTrace();
        }
        try{
        	Class c1 = 	urlCl.loadClass("com.utilify.platform.test.AdditionJob");
        	
        	System.out.println("Class found " + c1.getCanonicalName());
        } catch (Exception ex){
        	ex.printStackTrace();
        }
	}
}
