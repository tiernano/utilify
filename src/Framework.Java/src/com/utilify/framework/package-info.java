/**
 * The Utilify Framework package.
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://schemas.utilify.com/2007/09/Framework", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(value = com.utilify.framework.DateTypeAdapter.class, type = java.util.Date.class)
package com.utilify.framework;
//todoDoc: detailed docs for the package