
package com.utilify.framework;

/**
 * Thrown when a dependency with the specified Id is not found.
 */
public class DependencyNotFoundException extends FrameworkException {

	private static final long serialVersionUID = 7531085756455345861L;
	private String dependencyId = null;

    /**
     * Constructs a DependencyNotFoundException with the specified detail message and dependency Id.
     * @param message - the detail message.
     * @param dependencyId - the Id of the dependency that could not be found.
     */
    public DependencyNotFoundException(String message, String dependencyId){
    	super (String.format("%1$s. Dependency id : '%2$s'", message, dependencyId));
        this.dependencyId = dependencyId;
    }
    
    /**
     * Constructs a DependencyNotFoundException with no detail message.
     */
    public DependencyNotFoundException(){
    	super();
    }
    /**
     * Constructs a DependencyNotFoundException with the specified detail message.
     * @param message - the detail message.
     */
    public DependencyNotFoundException(String message){
    	super(message);
    }
    /**
     * Constructs a DependencyNotFoundException with the specified detail message.
     * @param message - the detail message.
     * @param cause - the cause of the exception.
     */
    public DependencyNotFoundException(String message, Throwable cause){
    	super(message, cause);
    }
    
    /**
     * Gets the dependency Id value that could not be found
     * @return the dependency Id value
     */
    public String getDependencyId(){
    	return dependencyId;
    }
}