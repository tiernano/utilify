
package com.utilify.framework.client;

import java.io.Serializable;

import com.utilify.framework.ExecutionContext;

/**
 * The interface that represents an executable job. Any class that contains code to be executed on the distributed network
 * using this framework needs to implement the <code>Executable</code> interface.
 * The class that implements this interface will have code to execute, whose entry point is {@link #execute(ExecutionContext)}. This 
 * method is called on a remote machine where the job will eventually execute after being scheduled by the server / Manager.
 */
public interface Executable extends Serializable {
    /**
     * The entry point method for remote execution of this job.
     * @param context 
     * @throws Exception 
     */
	void execute(ExecutionContext context) throws Exception;
}