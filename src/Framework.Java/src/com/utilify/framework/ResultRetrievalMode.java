
package com.utilify.framework;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/*
 * <p>Java class for ResultRetrievalMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResultRetrievalMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BackToOrigin"/>
 *     &lt;enumeration value="StoreRemotely"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */

/**
 * Represents the retrieval mode for a result produced by execution of a job.
 * Retrieval modes can be one of the following: <br>
 * <li>Back to origin</li><br>
 * The result is retrieved from the point of execution and returned to the origin (that is the machine that submitted the job). 
 * <li>Store remotely</li><br>
 * The result is retrieved from the point of execution and stored on a remote server (specified when submitting the job).
 * Currently the following remote protocols are supported: None.
 */
@XmlType(name = "ResultRetrievalMode")
@XmlEnum
public enum ResultRetrievalMode {

    /**
     * The retrieval mode for 'back to origin'.
     */
    @XmlEnumValue("BackToOrigin")
    BACK_TO_ORIGIN("BackToOrigin"),
    /**
     * The retrieval mode for 'store remotely'.
     */
    @XmlEnumValue("StoreRemotely")
    STORE_REMOTELY("StoreRemotely");
    private final String value;

    ResultRetrievalMode(String v) {
        value = v;
    }

    /**
     * Gets the value of the <code>ResultRetrievalMode</code>
     * @return the value of the mode (as a String).
     */
    public String value() {
        return value;
    }

    /**
     * Parses the specified value to create an <code>ResultRetrievalMode</code> object.
     * @param v - the value from which to create the <code>ResultRetrievalMode</code>.
     * @return the <code>ResultRetrievalMode</code> object
     * @throws IllegalArgumentException - if the specified value is not one of the acceptable mode values.
     */
    public static ResultRetrievalMode fromValue(String v) {
        for (ResultRetrievalMode c: ResultRetrievalMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
