package com.utilify.framework.dependency;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Stack;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

class ClassBuilder {

    private ClassParser parser;
    private ArrayList<String> packagesToExclude;
    
    //Key (String) = full class name
    //Value (JavaClass) = reference to the JavaClass object for the class
    private HashMap<String, JavaClass> classMap;

    //private static Logger logger = Logger.getLogger(ClassBuilder.class.getName());
    
    /**
     * Creates an instance of the <code>ClassBuilder</code> class
     */
    ClassBuilder(ArrayList<String> packagesToExclude) {
        if (packagesToExclude != null)
        	this.packagesToExclude = packagesToExclude;
        else
        	this.packagesToExclude = new ArrayList<String>();
        
        classMap = new HashMap<String, JavaClass>();
        this.parser = new ClassParser(this);
    }
    
	/**
	 * Gets the location of the class file for the specified class.
	 * The location of the class is determined from the @link {@link ProtectionDomain} of the 
	 * specified class. The returned location points to the path of the .class file or the .jar file containing
	 * the class file.
	 * @param clazz - the class whose location needs to be determined.
	 * @throws IllegalArgumentException - if <code>clazz</code> is a null reference.
	 * @return the location of the class file.
	 */
	static String getLocation(Class<? extends Object> clazz){
		if (clazz == null)
			throw new IllegalArgumentException("Class cannot be null");
		
		URL locationUrl = null;
		//we can detect stuff only if the classes are on the local disk
		CodeSource csrc = clazz.getProtectionDomain().getCodeSource();
		if (csrc == null){
			//We can still try to get the location using getResource 
			//System.out.println("dotToSlashes: " + dotsToSlashes(clazz.getName()) + ".class");
			ClassLoader cl = clazz.getClassLoader();
			if (cl != null){
				locationUrl = cl.getResource(dotsToSlashes(clazz.getName()) + ".class");
				//System.out.println("Location is " + locationUrl);
			} //else class loader is null -> could be the bootstrap classloader. ignore.
		} else {
			locationUrl = csrc.getLocation();
		}
		
		//this can be null if the class is one of the built-in runtime classes.
		if (locationUrl == null)
			return null;
		
		String location = locationUrl.toString();
		if (location.startsWith("file")){
			location = locationUrl.getPath();
		}
		
		if (!location.endsWith("jar") && !location.endsWith("war") && !location.endsWith("zip")){
			//it could be a class directory
			File f = new File(location);
			if (f.isDirectory()){
				//get the name of the class
				String actualDir = clazz.getName().replace('.', File.separatorChar);
				location = location + File.separatorChar + actualDir + ".class";
			} else {
				//TODO: will this ever happen?
				throw new IllegalArgumentException("Location is not a jar/war/zip file and is not a directory: " + location);
			}
		} //if location is a jar file, directly return it
		return location;
	}
	
    private static String dotsToSlashes(String name) {
		if (name != null)
			return name.replace('.', '/');
		return null;
	}

	/**
     * Builds the <code>JavaClass</code> instances from the 
     * specified file.
     * 
     * @param file Class or Jar file.
     * @return Collection of <code>JavaClass</code> instances.
     * @throws IOException
     * 	if there was an I/O error reading the file.
     */
    private Collection<JavaClass> buildClasses(File file) throws IOException {

        if (isValidClassFile(file)) {
            JavaClass parsedClass = parser.parse(file);
            parsedClass.setSource(file);
            Collection<JavaClass> javaClasses = new ArrayList<JavaClass>();
            javaClasses.add(parsedClass);
            return javaClasses;
        } else if (isValidJarFile(file)) {

            JarFile jarFile = new JarFile(file);
            Collection<JavaClass> result = buildClasses(file, jarFile);
            jarFile.close();
            return result;

        } else {
            throw new IOException("File is not a valid " + 
                ".class, .jar, .war, or .zip file: " + 
                file.getPath());
        }
    }

    private boolean isValidJarFile(File file) {
		if (file != null && file.isFile()){
			String name = file.getName().toLowerCase();
			return (name.endsWith(".jar") || name.endsWith(".war") || name.endsWith(".zip"));
		}
		return false;
	}

	private boolean isValidClassFile(File file) {
		if (file != null && file.isFile() && file.getName().toLowerCase().endsWith(".class")){
			return true;
		}
		return false;
	}
	
	private boolean isValidClassFileName(String name) {
		if (name != null && name.toLowerCase().endsWith(".class")){
			return true;
		}
		return false;
	}
	
    boolean shouldFilter(String referencedClass) {
    	for (String name : packagesToExclude) {
			if (referencedClass.toLowerCase().startsWith(name))
				return true;
		}
		return false;
	}

	/**
     * Builds the <code>JavaClass</code> instances from the specified 
     * jar, war, or zip file.
     * 
     * @param file Jar, war, or zip file.
     * @return Collection of <code>JavaClass</code> instances.
     * @throws IOException
     * 	if there was an I/O error reading the file.
     */
    private Collection<JavaClass> buildClasses(File source, JarFile file) throws IOException {

        Collection<JavaClass> javaClasses = new ArrayList<JavaClass>();

        Enumeration entries = file.entries();
        while (entries.hasMoreElements()) {
            ZipEntry e = (ZipEntry) entries.nextElement();
            if (isValidClassFileName(e.getName())) {
            	if (!shouldFilter(parser.slashesToDots(e.getName()))){
	                InputStream is = null;
	                try {
	                    is = file.getInputStream(e);
	                    JavaClass jc = parser.parse(e.getName(), is);
	                    jc.setSource(source);
	                    javaClasses.add(jc);
	                } finally {
	            		if (is != null)
	            			is.close();
	                }
            	}
            }
        }

        return javaClasses;
    }

	JavaClass buildReferenceTree(Class<? extends Object> clazz) throws IOException {
		
		String location = getLocation(clazz);
		if (location == null)
			return null; //nothing we can do here.
		
		Collection<JavaClass> classes = buildClasses(new File(location));
		//logger.fine("built classes for " + clazz.getName() + ", size = " + classes.size());
		
		Stack<JavaClass> classesToProcess = new Stack<JavaClass>();
		for (JavaClass class1 : classes){
			if (!classMap.containsKey(class1.getName())){
				classesToProcess.push(class1);
			}
		}
		
		while (classesToProcess.size() > 0){
			
			JavaClass class1 = classesToProcess.pop();
			classMap.put(class1.getName(), class1);
			
			String[] referencedClasses = class1.getReferencedClasses();
			
			//logger.fine("Processing class " + class1.getName() + 
				//	", referenced class count = " + referencedClasses.length);
			//System.out.println("Put in class : " + class1.getName());
			for (String referencedClass : referencedClasses){
				
				//System.out.println("\tref: " + referencedClass);
				
				if (shouldFilter(referencedClass))
					continue;
				
				if (!classMap.containsKey(referencedClass)){
					try {
						//load the class
						Class<? extends Object> refClass = Class.forName(referencedClass);
						//get its location
						location = getLocation(refClass);
						//build its own class tree
						if (location != null){
							classes = buildClasses(new File(location)); 
							for (JavaClass class2 : classes){
								if (!classMap.containsKey(class2.getName()))
									classesToProcess.push(class2);
							}
						} else {
							//location is null for referenced-class.
							//this happens for built-in runtime classes (JRE).
							//just ignore this.
						}
					} catch (ClassNotFoundException e) {
						throw new IOException(e.getMessage());
					}
				} 
				
			}
		}

		JavaClass jClass = classMap.get(clazz.getName());
		//logger.fine("Getting jclass for " + clazz.getName() + ", is null? = " + (jClass == null));
		return jClass;
	}

	/**
	 * Gets a JavaClass instance corresponding to the given class name
	 * @param className : name of the class
	 * @return the JavaClass instance for the given class name
	 */
	JavaClass getJavaClass(String className) {
		if (className != null && classMap.containsKey(className)){
			return classMap.get(className);
		}
		return null;
	}

	Collection<JavaClass> getClassesBuilt(){
		return classMap.values();
	}
	
	ArrayList<String> getPackagesToExclude() {
		return packagesToExclude;
	}
}
