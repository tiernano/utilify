
package com.utilify.framework.manager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.utilify.framework.client.ArrayOfResultStatusInfo;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetJobResultsResult" type="{http://schemas.utilify.com/2007/09/Client}ArrayOfResultStatusInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getJobResultsResult"
})
@XmlRootElement(name = "GetJobResultsResponse")
public class GetJobResultsResponse {

    @XmlElement(name = "GetJobResultsResult", nillable = true)
    protected ArrayOfResultStatusInfo getJobResultsResult;

    /**
     * Gets the value of the getJobResultsResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfResultStatusInfo }
     *     
     */
    public ArrayOfResultStatusInfo getGetJobResultsResult() {
        return getJobResultsResult;
    }

    /**
     * Sets the value of the getJobResultsResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfResultStatusInfo }
     *     
     */
    public void setGetJobResultsResult(ArrayOfResultStatusInfo value) {
        this.getJobResultsResult = value;
    }

}
