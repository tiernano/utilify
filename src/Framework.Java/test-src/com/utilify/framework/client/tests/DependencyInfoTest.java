package com.utilify.framework.client.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Assert;
import org.junit.Test;

import com.utilify.framework.DependencyType;
import com.utilify.framework.client.DependencyInfo;
import com.utilify.framework.dependency.DependencyResolver;

public class DependencyInfoTest {

	/*
	 * [Category("DependencyInfo")]
	 */
	@Test
	public void DependencyInfoCtor1() throws IOException, MalformedURLException {
		// correct instantiation
		DependencyInfo di = new DependencyInfo("aName", DependencyResolver.getLocation(this.getClass()),
				DependencyType.JAVA_MODULE);
		Assert.assertNotNull("The name should not be null", di.getName());
		Assert.assertNotNull("The file name should not be null", di.getFilename());
		Assert.assertNotNull("The hash should not be null", di.getHash());
		Assert.assertTrue("The size should not be a non-negative integer", (di.getSize() >= 0));
		//since Java dates are UTC already - no need to test for that.
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyInfoNullNameCtor1() throws IOException, MalformedURLException {
		//first argument should raise an exception
		new DependencyInfo(null, DependencyResolver.getLocation(this.getClass()), DependencyType.JAVA_MODULE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyInfoEmptyNameCtor1() throws IOException, MalformedURLException {
		// first argument should raise an exception
		new DependencyInfo(" ", DependencyResolver.getLocation(this.getClass()), DependencyType.JAVA_MODULE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyInfoNullFilePathCtor1() throws IOException, MalformedURLException {
		// second argument should raise an exception
		new DependencyInfo("aName", null, DependencyType.JAVA_MODULE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void createDependencyInfoEmptyFilePathCtor1() throws IOException, MalformedURLException {
		// second argument should raise an exception
		new DependencyInfo("aName", " ", DependencyType.JAVA_MODULE);
	}

	@Test(expected = FileNotFoundException.class)
	public void createDependencyInfoWithNonExistentFilePathCtor1() throws IOException, MalformedURLException {
		// second argument should raise an exception
		new DependencyInfo("aName", "a random string here", DependencyType.JAVA_MODULE);
	}
	
	@Test
	public void createDependencyInfoWithUrlTypeCtor1() throws IOException, MalformedURLException {
		new DependencyInfo("aName", "http://ahost.com/afile", DependencyType.REMOTE_URL);
		// no assertions - basically the ctor should not throw any excpetions - for remote url
	}

	@Test(expected = MalformedURLException.class)
	public void createDependencyInfoWithBadUrlTypeCtor1() throws IOException, MalformedURLException {
		// url argument should raise an exception : since we have a bad url
		// for now we don't exclude unknown schemes.
		new DependencyInfo("aName", "myRandomUrl://\\ &?aHost.com//afile", DependencyType.REMOTE_URL);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void DependencyInfoFromNullClass() throws IOException, MalformedURLException {
		DependencyInfo.fromClass(null);
	}
	
	@Test
	public void DependencyInfoFromClass() throws IOException, MalformedURLException {
		DependencyInfo di =  DependencyInfo.fromClass(this.getClass());
		Assert.assertNotNull("The object returned by 'fromClass' should not be null.", di);

        Assert.assertTrue("The type should be 'JavaModule'", 
        		(di.getType() == DependencyType.JAVA_MODULE));
        
		//do the usual assertions
		Assert.assertNotNull("The name should not be null", di.getName());
		Assert.assertNotNull("The file name should not be null", di.getFilename());
		Assert.assertNotNull("The hash should not be null", di.getHash());
		Assert.assertTrue("The size should not be a non-negative integer", (di.getSize() >= 0));
		//since Java dates are UTC already - no need to test for that.
	}
}