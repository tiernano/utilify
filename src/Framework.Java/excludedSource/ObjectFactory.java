
package com.utilify.framework;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.utilify.framework package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EntityType_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "EntityType");
    private final static QName _PriorityLevel_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "PriorityLevel");
    private final static QName _JobStatus_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "JobStatus");
    private final static QName _ArgumentFault_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "ArgumentFault");
    private final static QName _DependencyScope_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "DependencyScope");
    private final static QName _ApplicationStatus_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "ApplicationStatus");
    private final static QName _JobType_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "JobType");
    private final static QName _DependencyType_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "DependencyType");
    private final static QName _ResultRetrievalMode_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "ResultRetrievalMode");
    private final static QName _EntityNotFoundFault_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "EntityNotFoundFault");
    private final static QName _InvalidOperationFault_QNAME = new QName("http://schemas.utilify.com/2007/09/Framework", "InvalidOperationFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.utilify.framework
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EntityNotFoundFault }
     * 
     */
    public EntityNotFoundFault createEntityNotFoundFault() {
        return new EntityNotFoundFault();
    }

    /**
     * Create an instance of {@link InvalidOperationFault }
     * 
     */
    public InvalidOperationFault createInvalidOperationFault() {
        return new InvalidOperationFault();
    }

    /**
     * Create an instance of {@link ArgumentFault }
     * 
     */
    public ArgumentFault createArgumentFault() {
        return new ArgumentFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "EntityType")
    public JAXBElement<EntityType> createEntityType(EntityType value) {
        return new JAXBElement<EntityType>(_EntityType_QNAME, EntityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriorityLevel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "PriorityLevel")
    public JAXBElement<PriorityLevel> createPriorityLevel(PriorityLevel value) {
        return new JAXBElement<PriorityLevel>(_PriorityLevel_QNAME, PriorityLevel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "JobStatus")
    public JAXBElement<JobStatus> createJobStatus(JobStatus value) {
        return new JAXBElement<JobStatus>(_JobStatus_QNAME, JobStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArgumentFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "ArgumentFault")
    public JAXBElement<ArgumentFault> createArgumentFault(ArgumentFault value) {
        return new JAXBElement<ArgumentFault>(_ArgumentFault_QNAME, ArgumentFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyScope }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "DependencyScope")
    public JAXBElement<DependencyScope> createDependencyScope(DependencyScope value) {
        return new JAXBElement<DependencyScope>(_DependencyScope_QNAME, DependencyScope.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "ApplicationStatus")
    public JAXBElement<ApplicationStatus> createApplicationStatus(ApplicationStatus value) {
        return new JAXBElement<ApplicationStatus>(_ApplicationStatus_QNAME, ApplicationStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "JobType")
    public JAXBElement<JobType> createJobType(JobType value) {
        return new JAXBElement<JobType>(_JobType_QNAME, JobType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DependencyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "DependencyType")
    public JAXBElement<DependencyType> createDependencyType(DependencyType value) {
        return new JAXBElement<DependencyType>(_DependencyType_QNAME, DependencyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultRetrievalMode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "ResultRetrievalMode")
    public JAXBElement<ResultRetrievalMode> createResultRetrievalMode(ResultRetrievalMode value) {
        return new JAXBElement<ResultRetrievalMode>(_ResultRetrievalMode_QNAME, ResultRetrievalMode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntityNotFoundFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "EntityNotFoundFault")
    public JAXBElement<EntityNotFoundFault> createEntityNotFoundFault(EntityNotFoundFault value) {
        return new JAXBElement<EntityNotFoundFault>(_EntityNotFoundFault_QNAME, EntityNotFoundFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidOperationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.utilify.com/2007/09/Framework", name = "InvalidOperationFault")
    public JAXBElement<InvalidOperationFault> createInvalidOperationFault(InvalidOperationFault value) {
        return new JAXBElement<InvalidOperationFault>(_InvalidOperationFault_QNAME, InvalidOperationFault.class, null, value);
    }

}
