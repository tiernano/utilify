using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Utilify.Platform.Executor
{
    internal class JobReturner : PlatformServiceBase
    {
        private IJobManager proxy;
        private JobTable jobTable;
        private String executorId;
        private EventWaitHandle stoppedJobEvent;
        private int pollInterval = 100;

        private Dictionary<ResultRetrievalMode, IResultUploader> uploaders =
            new Dictionary<ResultRetrievalMode, IResultUploader>();        

        //max-retry for sending jobs/results
        private const int sendRetry = 5;

        internal JobReturner(JobTable jobTable)
        {
            if (jobTable == null)
                throw new ArgumentNullException("jobTable");

            this.jobTable = jobTable;
            this.stoppedJobEvent = new AutoResetEvent(true);
        }

        // todoLater, todoDiscuss: After sending back the completion info for a job, the returner will attempt to return the results 
        //       to the Manager. If the returner here fails to send back results (maybe the result file was never produced) then
        //       the manager will never close off the job even if on the Executor side the Job has been finished with already.
        //       A solution could be to have another message to finalise the Job telling the Manager that the Job is done with
        //       on the Executor's side. This could also reduce the burden on the Manager by reducing the number of calls to
        //       check if all of a Jobs results have been returned. Could be used by the client on Job submission to reduce need
        //       to check dependencies for ready jobs..
        protected override void Run()
        {
            CheckValidState();

            while (true)
            {
                try
                {
                    stoppedJobEvent.WaitOne(pollInterval, false);

                    if (IsStopRequested)
                        break;

                    // Get all Stopped Jobs to be sent back to the Manager.
                    Job[] jobs = jobTable.GetJobs(ExecutionStatus.Stopped);
                    if (jobs != null && jobs.Length > 0)
                    {
                        logger.Info("Got {0} Jobs to Return.", jobs.Length);
                        foreach (Job job in jobs)
                        {
                            logger.Debug("Returning Jobs...");

                            // Check if we should ignore this job
                            if (job.IsCancelled)
                                continue;

                            // Try send results first so that any errors can be put into the Job error log.
                            if (TryVerifyResults(job))
                            {
                                logger.Debug("Results sent successfully.");
                            }
                            else
                            {
                                logger.Debug("Some results were not sent.");
                            }
                            // Regardless of whether sending results fails, we still need to send back the Job.
                            // todo: We could use the Job.FailureCount to allow this to retry a couple of times before failing.
                            if (TrySendJob(job))
                            {
                                logger.Debug("Successfully sent Job back to Manager");
                            }
                            else
                            {
                                logger.Info("Failed to send Job back to Manager");
                            }

                            jobTable.Remove(job);
                            logger.Debug(String.Format("Removed Job {0} from JobTable.", job.JobId));
                        }
                    }
                }
                catch (InternalServerException ix) //insulate exctor from the Manager's exceptions
                {
                    logger.Warn("Server exception: ", ix);
                    Thread.Sleep(BackOff.GetNextInterval()); //back off for a bit
                }
                catch (Exception e)
                {
                    OnError(e, true);
                    break;
                }
            } // while
        }

        private bool TrySendJob(Job job)
        {
            // Create the Completion Info from the Job.
            if (job.FinalJobInstance == null)
                job.FinalJobInstance = job.InitialJobInstance;

            JobCompletionInfo info = job.ToJobCompletionInfo();
            logger.Debug("Created completion info...");
            logger.Debug("Job log :\n {0}", info.ExecutionLog);
            logger.Debug("Job error :\n {0}", info.ExecutionError);

            bool sentJob = false;
            while (!sentJob)
            {
                try
                {
                    // Check if we should continue
                    if (job.IsCancelled)
                        break;

                    // Send Completed Job/Jobs
                    proxy.SendCompletedJob(info);
                    logger.Debug("Sent completion info for job {0}", job.JobId);
                    sentJob = true;
                }
                catch (Utilify.Platform.CommunicationException cx)
                {
                    if (BackOff.NumberOfRetries > Math.Min(sendRetry, BackOff.MaxRetries))
                        break;

                    //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                    int backOffInterval = BackOff.GetNextInterval();
                    logger.Warn("Could not send completed Job {0}, due to error :\n{1}\n. Backing off for {2}.",
                        info.JobId, cx, Helper.GetFormattedTimeSpan(backOffInterval));

                    Thread.Sleep(backOffInterval);
                }
            }

            return sentJob;
        }

        /// <summary>
        /// Tries to send the existing results to the manager.
        /// If a result file is not found, it logs a warning, and continues with other files.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private bool TryVerifyResults(Job job)
        {
            bool sentAllResults = false;
            
            List<string> resultSuccess = new List<string>();

            ResultInfo[] results = job.GetExpectedResults();

            logger.Debug(String.Format("Sending {0} results... ", results.Length));
            foreach (ResultInfo result in results)
            {
                if (job.IsCancelled)
                    break;

                if (!TrySendResult(job, result))
                {
                    sentAllResults = false;
                    //break; //todo decide: do we stop sending or continue with other results!?
                }
                else
                {
                    resultSuccess.Add(result.Id);
                }
            }

            //check if each result has been sent
            if (results.Length == resultSuccess.Count)
            {
                //all results sent successfully
                sentAllResults = true;
            }

            return sentAllResults;
        }

        private bool TrySendResult(Job job, ResultInfo result)
        {
            IResultUploader uploader = GetResultUploader(result.RetrievalMode);

            string resFileName = result.FileName;
            if (result.RetrievalMode == ResultRetrievalMode.StoreRemotely)
            {
                try
                {
                    Uri uri = new Uri(resFileName);
                    //todoLater: if/when we support sub-directories on the Executor, we'll need all this code below:
                    //resFileName = uri.LocalPath.Replace('/', Path.DirectorySeparatorChar);
                    //fix up the file name
                    //if (resFileName.StartsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    resFileName = resFileName.Substring(1); //leave out the first character
                    //}

                    //for now, we don't allow sub-directories
                    resFileName = Path.GetFileName(uri.LocalPath);
                }
                catch (UriFormatException ufx)
                {
                    job.ExecutionError.AppendLine("Invalid uri format: " + resFileName);
                    logger.Warn("Invalid uri format for remote result file.", ufx);
                    return false;
                }
            }

            string localPath = Path.Combine(job.JobDirectory, resFileName);
            logger.Debug("**** path = {0}", localPath);

            bool sentResult = false;
            if (File.Exists(localPath))
            {
                while (!sentResult)
                {
                    // Check if we should continue
                    if (job.IsCancelled)
                        break;

                    try
                    {
                        sentResult = uploader.PutResult(job.JobId, localPath, result);
                        sentResult = true;
                    }
                    catch (InvalidOperationException iox)
                    {
                        logger.Warn("Could not send result.", iox);
                        break;
                    }
                    catch (Utilify.Platform.CommunicationException cx)
                    {
                        if (BackOff.NumberOfRetries > Math.Min(sendRetry, BackOff.MaxRetries))
                            break;

                        //the proxy base will reset the proxy, and create a new communication channel for use in the next round
                        int backOffInterval = BackOff.GetNextInterval();
                        logger.Warn("Could not send Result {0}. Backing off for {1}.",
                            cx, result.Id, Helper.GetFormattedTimeSpan(backOffInterval));

                        Thread.Sleep(backOffInterval);
                    }
                }
            }
            else
            {
                logger.Warn("Warning: Result will not be sent because the file was not found: " + localPath);
                job.ExecutionError.AppendLine(String.Format("Cannot store result. File not found {0}", result.FileName));
            }

            return sentResult;
        }

        private IResultUploader GetResultUploader(ResultRetrievalMode resultRetrievalMode)
        {
            IResultUploader uploader = null;
            if (uploaders.ContainsKey(resultRetrievalMode))
            {
                uploader = uploaders[resultRetrievalMode];
            }
            else
            {
                switch (resultRetrievalMode)
                {
                    case ResultRetrievalMode.BackToOrigin:
                        uploader = new SimpleResultUploader(this.proxy);
                        break;
                    case ResultRetrievalMode.StoreRemotely:
                        uploader = new RemoteResultUploader(InternalState.DataTransferService);
                        break;
                }

                uploaders[resultRetrievalMode] = uploader;
            }
            return uploader;
        }

        private void CheckValidState()
        {
            if (!string.IsNullOrEmpty(this.executorId))
                return;

            //get executor info from the internal state
            InternalState.WaitHandle.WaitOne();

            //if we still don't have a valid config, blow up:
            ExecutorConfiguration config = InternalState.ExecutorConfiguration;
            if (config != null)
                this.executorId = config.Id;

            if (string.IsNullOrEmpty(this.executorId))
                throw new InvalidOperationException("Executor configuration could not be retrieved");

            //now get the proxy 
            this.proxy = InternalState.JobManager;
        }

        internal void OnExecutionStatusChanged(object sender, ExecutionStatusEventArgs e)
        {
            switch (e.Status)
            {
                case ExecutionStatus.Stopped:
                    stoppedJobEvent.Set();
                    break;
                case ExecutionStatus.Cancelled:
                    // Cancel job.
                    break;
            }
        }

        protected internal override string Name
        {
            get { return "JobReturner"; }
        }
    }
}
