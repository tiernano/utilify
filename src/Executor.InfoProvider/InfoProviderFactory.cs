using Utilify.Platform;
using System;

namespace Utilify.Platform.Executor.InfoProvider
{
    public static class InfoProviderFactory
    {
        public static ISystemInfoProvider GetSystemInfoProvider()
        {
            if (OSInfoHelper.GetPlatform() == PlatformID.Unix)
            {
                return new Utilify.Platform.Executor.InfoProvider.Linux.LinuxSystemInfoProvider();
            }
            else
            {
                return new Utilify.Platform.Executor.InfoProvider.Win32.Win32SystemInfoProvider();
            }
        }

        public static IPerformanceInfoProvider GetPerformanceInfoProvider()
        {
            //return proper provider based on OS
            if (OSInfoHelper.GetPlatform() == PlatformID.Unix)
            {
                return new Utilify.Platform.Executor.InfoProvider.Linux.LinuxPerformanceInfoProvider();
            }
            else
            {
                return new Utilify.Platform.Executor.InfoProvider.Win32.Win32PerformanceInfoProvider();
            }            
        }
    }
}
