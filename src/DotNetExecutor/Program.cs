using System;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using System.Collections;

namespace Utilify.Platform.DotNetExecutor
{
    class Program
    {
        private static log4net.ILog log = null;

        private const string netExecutorSectionName = "utilify.platform.netExecutor";
        private const string executorServicePortKey = "executorServicePort";

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            log = log4net.LogManager.GetLogger(typeof(Program));
            try
            {
                //just to initialize the logger
                Logger.MessageLogged += new EventHandler<LogEventArgs>(Logger_MessageLogged_Log4Net);
                log.Debug("Starting Executor...");
                string argsString = "";
                foreach (string arg in args)
                {
                    argsString += (arg + " ");
                }
                log.Debug("Args: " + argsString);
                // Read any settings from command line passed 
                // by the Executor when launching this process.
                int port = DotNetExecutor.DefaultPort;
                bool gotPortFromArgs = false;
                if (args.Length == 2) // arg[0] = port, arg[1] = log directory (is it used?)
                {
                    if (Int32.TryParse(args[0], out port))
                    {
                        gotPortFromArgs = (port > 0);
                    }
                }

                // todoDiscuss: I understand this is like a fallback if the port isn't provided from the Dispatcher, but I think
                //       there should really never be a case where the Dispatcher doesn't give the port.
                //       The problem with having the setting in the app.config is that it may confuse someone who thinks that
                //       changing that value actually does something when in reality the value should
                //       never even be read.
                if (!gotPortFromArgs)
                {
                    //get port# from config file
                    try
                    {
                        IDictionary values = ConfigurationManager.GetSection(netExecutorSectionName) as IDictionary;
                        if (values != null)
                        {
                            if (values.Contains(executorServicePortKey))
                                Int32.TryParse((string)values[executorServicePortKey], out port);
                        }
                    }
                    catch (ConfigurationErrorsException cx) 
                    {
                        log.Warn("Could not get from config file. Error :" + cx.Message);
                    }
                }

                log.Info("Starting DotNetExecutor to connect back on port: " + port);
                DotNetExecutor ex = new DotNetExecutor(port);
                ex.Start();
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString());
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (log != null)
            {
                Exception ex = (Exception)e.ExceptionObject;
                log.Fatal("Application terminating = " + e.IsTerminating + ". Fatal unhandled error from " + sender,
                             ex);
            }
        }

        //captures all framework log messages
        static void Logger_MessageLogged_Log4Net(object sender, LogEventArgs e)
        {
            string format = "<{0}:{1}> {2} - {3} {4}";
            switch (e.Level)
            {
                case LogLevel.Debug:
                    log.DebugFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Info:
                    log.InfoFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
                case LogLevel.Warn:
                case LogLevel.Error:
                    log.ErrorFormat(format,
                        Path.GetFileName(e.StackFrame.GetFileName()), e.StackFrame.GetFileLineNumber(), e.StackFrame.GetMethod().Name,
                        e.Message, e.Exception);
                    break;
            }
        }
    }
}
