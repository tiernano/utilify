#if !MONO

using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Runtime.Remoting;
using System.IO;

namespace Utilify.Platform
{
    internal static class FaultMapper
    {
        internal static Exception GetException(FaultException fx)
        {
            Exception ex = null;
            if (fx == null)
                return null;

            if (IsFaultOfType<ArgumentFault>(fx))
            {
                ArgumentFault argFault = GetFault<ArgumentFault>(fx);
                if (argFault.IsValueNull)
                {
                    ex = new ArgumentNullException(argFault.ParameterName, argFault.Message);
                }
                else
                {
                    ex = new ArgumentException(argFault.Message, argFault.ParameterName);
                }
            }
            else if (IsFaultOfType<EntityNotFoundFault>(fx))
            {
                EntityNotFoundFault ef = GetFault<EntityNotFoundFault>(fx);
                switch (ef.EntityType)
                {
                    case EntityType.Application:
                        ex = new ApplicationNotFoundException(ef.Message, ef.EntityId);
                        break;
                    case EntityType.Job:
                        ex = new JobNotFoundException(ef.Message, ef.EntityId);
                        break;
                    case EntityType.Dependency:
                        ex = new DependencyNotFoundException(ef.Message, ef.EntityId);
                        break;
                    case EntityType.Result:
                        ex = new ResultNotFoundException(ef.Message, ef.EntityId);
                        break;                    
                    case EntityType.Directory:
                        ex = new DirectoryNotFoundException(ef.Message);
                        break;
                    case EntityType.File:
                        ex = new FileNotFoundException(ef.Message);
                        break;
                    default:
                        ex = new PlatformException(ef.Message); //todoLater: perhaps we should include Group/User/Executor not found exceptions here as well.
                        break;
                }
            }
            else if (IsFaultOfType<InvalidOperationFault>(fx))
            {
                InvalidOperationFault iof = GetFault<InvalidOperationFault>(fx);
                ex = new InvalidOperationException(iof.Message);
            }
            else if (IsFaultOfType<AuthenticationFault>(fx))
            {
                AuthenticationFault af = GetFault<AuthenticationFault>(fx);
                ex  = new System.Security.Authentication.AuthenticationException(af.Message);                
            }
            else if (IsFaultOfType<AuthorizationFault>(fx))
            {
                AuthorizationFault af = GetFault<AuthorizationFault>(fx);
                ex = new UnauthorizedAccessException(af.Message);
            }
            else if (IsFaultOfType<ServerFault>(fx))
            {
                ServerFault sf = GetFault<ServerFault>(fx);
                ex = new InternalServerException(string.Format("{0}:{1}{2}", sf.Message, Environment.NewLine, sf.Description));
            }
            else
            {
                ex = fx; //return what we got back
            }
            return ex;
        }

        private static bool IsFaultOfType<T>(FaultException fx)
        {
            return (fx.GetType() == typeof(FaultException<T>));
        }

        private static T GetFault<T>(FaultException fx)
        {
            return ((FaultException<T>)fx).Detail;
        }
    }
}

#endif