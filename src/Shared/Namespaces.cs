#if !MONO

using Utilify.Platform;
using System.Runtime.Serialization;

//WCF serialization attributes
[assembly: ContractNamespace(Namespaces.Client, ClrNamespace = "Utilify.Framework.Client")]
[assembly: ContractNamespace(Namespaces.Cloud, ClrNamespace = "Utilify.Framework")]
[assembly: ContractNamespace(Namespaces.Executor, ClrNamespace = "Utilify.Framework.Executor")]
[assembly: ContractNamespace(Namespaces.Framework, ClrNamespace = "Utilify.Framework")]
[assembly: ContractNamespace(Namespaces.Manager, ClrNamespace = "Utilify.Framework.Manager")]
[assembly: ContractNamespace(Namespaces.Platform, ClrNamespace = "Utilify.Platform")]

#endif

namespace Utilify.Platform
{

    internal static class Namespaces
    {
        private const string baseNs = "http://schemas.utilify.com/2007/09/";

        public const string Cloud = baseNs + "Cloud";
        public const string ManagerCompat = baseNs + "ManagerCompat";
        public const string Manager = baseNs + "Manager";
        public const string Executor = baseNs + "Executor";
        public const string Client = baseNs + "Client";
        public const string Framework = baseNs + "Framework";
        public const string Platform = baseNs + "Platform";
    }
}