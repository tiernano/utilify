using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Utilify.Platform
{
    internal static class SerializationHelper
    {
        #region (De-)Serialization helpers

        internal static byte[] Serialize(object theJob)
        {
            //todo: abstract this out to use whatever serializer : to perhaps conditionally compress / encrypt etc
            //for now we're just using a binary one
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binfm =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                binfm.Serialize(ms, theJob);
                return ms.ToArray();
            }
        }

        internal static object Deserialize(byte[] instance)
        {
            if (instance == null)
                throw new ArgumentNullException("instance");

            if (instance.Length == 0)
                throw new ArgumentException("InitialInstance data cannot be zero length.", "instance");

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binfm =
                new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(instance))
            {
                return binfm.Deserialize(ms);
            }
        }

        #endregion
    }
}
