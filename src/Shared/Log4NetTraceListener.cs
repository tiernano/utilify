namespace Utilify.Platform
{
    class Log4netTraceListener : System.Diagnostics.TraceListener 
    { 
        private static log4net.ILog logger; 

        public Log4netTraceListener(log4net.ILog logger) 
        { 
            Log4netTraceListener.logger = logger;
            // = log4net.LogManager.GetLogger("Utilify");
        } 

        public override void Write(string message) 
        {
            if (logger != null)
                logger.Debug(message);
        } 

        public override void WriteLine(string message) 
        { 
            if (logger != null)
                logger.Debug(message);
        } 
    }
}