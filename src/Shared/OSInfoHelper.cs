﻿using System;

namespace Utilify.Platform.Executor.InfoProvider
{
    internal static class OSInfoHelper
    {
        /// <summary>
        /// Gets the current operating system platform.
        /// </summary>
        /// <returns></returns>
        public static PlatformID GetPlatform()
        {
            //Need to do both checks, since Mono and .NET CLR have different enum values for 'Unix'. 
            int p = (int)Environment.OSVersion.Platform;
            if ((p == 4) || (p == 128))
            {
                return PlatformID.Unix; //on mono, Unix enum value is different
            }
            else
            {
                return Environment.OSVersion.Platform;
            }
        }
    }
}
