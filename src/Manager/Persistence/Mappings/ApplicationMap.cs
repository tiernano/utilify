﻿using FluentNHibernate.Mapping;

namespace Utilify.Platform.Manager.Persistence.Mappings
{
    internal class ApplicationMap : ClassMap<Application>
    {
        public ApplicationMap()
        {
            this.Id(x => x.Id).TheColumnNameIs("Id");
        }
    }
}
