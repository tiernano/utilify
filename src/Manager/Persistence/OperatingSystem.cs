using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using Utilify.Platform.Contract;

namespace Utilify.Platform.Manager.Persistence
{
    [Serializable]
    public class OperatingSystem : ITransformable<OSInfo>
    {
        private string name;
        private string version;

        private OperatingSystem() { }

        public OperatingSystem(OSInfo info) : this()
        {
            this.Name = info.Name;
            this.Version = info.Version;
        }

        public string Name
        {
            get { return name; }
            private set
            {
                this.name = value;
            }
        }

        public string Version
        {
            get { return version; }
            private set
            {
                this.version = value;
            }
        }

        #region ITransformable<OSInfo> Members

        OSInfo ITransformable<OSInfo>.Transform()
        {
            return new OSInfo(this.Name, this.Version);
        }

        #endregion
    }
}
