﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using RayTracer;
using Utilify.Framework;

namespace Utilify.Platform.Demo.UtilifyRayTracer
{
    [Serializable]
    public class UtilifyRayTracerJob : IExecutable
    {
        private Bitmap image;
        private Scene scene;
        private Rectangle viewport;
        private int startRow;
        private int numberOfRowsToTrace;
        private AntiAliasing antiAliasing;
        private bool renderDiffuse = true;
        private bool renderHighlights = true;
        private bool renderReflections = true;
        private bool renderRefractions = true;
        private bool castShadows = true;

        public UtilifyRayTracerJob(Scene scene, Rectangle viewport, int startRow, int numberOfRowsToTrace)
        {
            this.scene = scene;
            this.viewport = viewport;
            this.startRow = startRow;
            this.numberOfRowsToTrace = numberOfRowsToTrace;
        }

        #region Properties

        public Bitmap Image
        {
            get { return image; }
            private set { this.image = value; }
        }

        public Scene Scene
        {
            get { return scene; }
//            set { scene = value; }
        }

        public Rectangle Viewport
        {
            get { return viewport; }
//            set { viewport = value; }
        }

        public int StartRow
        {
            get { return startRow; }
//            set { startRow = value; }
        }

        public int NumberOfRowsToTrace
        {
            get { return numberOfRowsToTrace; }
//            set { numberOfRowsToTrace = value; }
        }

        public AntiAliasing AntiAliasing
        {
            get { return antiAliasing; }
            set { antiAliasing = value; }
        }

        public bool RenderDiffuse
        {
            get { return renderDiffuse; }
            set { renderDiffuse = value; }
        }

        public bool RenderHighlights
        {
            get { return renderHighlights; }
            set { renderHighlights = value; }
        }

        public bool RenderReflections
        {
            get { return renderReflections; }
            set { renderReflections = value; }
        }

        public bool RenderRefractions
        {
            get { return renderRefractions; }
            set { renderRefractions = value; }
        }

        public bool CastShadows
        {
            get { return castShadows; }
            set { castShadows = value; }
        }

        #endregion

        #region IExecutable Members

        public void Execute(Utilify.Framework.ExecutionContext context)
        {
            RayTracer.RayTracer raytracer = new RayTracer.RayTracer(this.AntiAliasing,
                                                    this.RenderDiffuse,
                                                    this.RenderHighlights,
                                                    this.CastShadows,
                                                    this.RenderReflections,
                                                    this.RenderRefractions);

            this.Image = raytracer.RayTraceRows(this.scene, this.viewport, this.startRow, this.numberOfRowsToTrace);
            // For Debugging output the image: this is not really necessary
            //this.Image.Save(System.IO.Path.Combine(context.WorkingDirectory,"output" + startRow + ".jpg"));
        }

        #endregion
    }
}
