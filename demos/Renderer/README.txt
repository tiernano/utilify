
This is just some instructions on what needs to be done to get this running.

Setting up Environment:
-----------------------

- Download and Install Yafray:
http://www.yafray.org/index.php?s=2

- Set Windows Environment Variable YAFRAY_HOME:
So that the executor can find where the exe is, set an environment var YAFRAY_HOME to point to the directory containing yafray.exe .
It can be set as either a User or Machine level variable.

Running:
-------

- When running, the app will look for directories within the current executing directory and display them as scenes. Each of the directories should
contain all the files needed to render a particular scene. Currently you should find 2 scenes 'monkey' and 'spheres'. All files will be sent as dependencies
and used during the render.

- To test the rendering on the local machine, click the 'render locally' checkbox

