using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Utilify.Framework.Client;
using Utilify.Framework;

namespace Utilify.Platform.Demo.RendererLibrary
{
    [Serializable]
    public class YafrayJob
    {
        private string inputFile;
        private int imageWidth;
        private int imageHeight;
        private int segmentWidth;
        private int segmentHeight;
        private int column;
        private int row;
        private Bitmap imageSegment;

		public YafrayJob(
                string inputFile,
                int imageWidth,
                int imageHeight,
                int segmentWidth, 
                int segmentHeight, 
                int column,
                int row
                )
		{
			this.inputFile = inputFile;
            this.imageWidth = imageWidth;
            this.imageHeight = imageHeight;
			this.segmentWidth = segmentWidth;
			this.segmentHeight = segmentHeight;
			this.column = column;
            this.row = row;
        }

        #region Properties

        public string InputFile
        {
            get { return inputFile; }
            set { inputFile = value; }
        }

        public int ImageWidth
        {
            get { return imageWidth; }
            set { imageWidth = value; }
        }

        public int ImageHeight
        {
            get { return imageHeight; }
            set { imageHeight = value; }
        }

        public int SegmentWidth
        {
            get { return segmentWidth; }
            set { segmentWidth = value; }
        }

        public int SegmentHeight
        {
            get { return segmentHeight; }
            set { segmentHeight = value; }
        }

        public int Column
        {
            get { return column; }
            set { column = value; }
        }

        public int Row
        {
            get { return row; }
            set { row = value; }
        }

        public Bitmap ImageSegment
        {
            get { return imageSegment; }
            set { imageSegment = value; }
        }

        #endregion

        public void Render(string workingDirectory)
		{
            // Required input files will be in the Application directory
            // Yafray executable is assumed to be in the PATH
            Console.WriteLine("segmentWidth:" + segmentWidth);
            Console.WriteLine("segmentHeight:" + segmentHeight);
            int x = column * segmentWidth;
            int y = row * segmentHeight;
            double xmin = YafrayHelper.ToYafrayCoordX(x, imageWidth);
            double xmax = YafrayHelper.ToYafrayCoordX(x + segmentWidth, imageWidth);
            double ymin = YafrayHelper.ToYafrayCoordY(y + segmentHeight, imageHeight);
            double ymax = YafrayHelper.ToYafrayCoordY(y, imageHeight);
            
            Console.WriteLine("xmin:" + xmin);
            Console.WriteLine("xmax:" + xmax);

            // We cant do linking yet..
            CopySceneFiles(workingDirectory);

            string yafrayPath = System.Environment.GetEnvironmentVariable("YAFRAY_HOME", EnvironmentVariableTarget.User);
            if (yafrayPath == null)
                yafrayPath = System.Environment.GetEnvironmentVariable("YAFRAY_HOME", EnvironmentVariableTarget.Machine);

            string cmd = Path.Combine(yafrayPath, "yafray.exe");
            string args = string.Format(" -r {0}:{1}:{2}:{3} {4}",
                                        xmin,
                                        xmax,
                                        ymin,
                                        ymax,
                                        inputFile
                                        );


            Console.WriteLine("cmd: " + cmd);
            Console.WriteLine("args: " + args);

            // Call Yafray with required region arguments (output file will be a specific filename)
            Process yafray = new Process();
            yafray.StartInfo.FileName = cmd;
            yafray.StartInfo.Arguments = args;
            yafray.StartInfo.WorkingDirectory = workingDirectory;
            yafray.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            yafray.StartInfo.UseShellExecute = false;
            yafray.StartInfo.CreateNoWindow = true;

            yafray.StartInfo.RedirectStandardError = true;
            yafray.StartInfo.RedirectStandardOutput = true;

            Console.WriteLine("Rendering...");

            yafray.Start();
            yafray.WaitForExit();

            String stdOutPath = Path.Combine(workingDirectory,"yafray_stdout.txt");
            using (FileStream fs = File.Create(stdOutPath)) { }
            using (FileStream fs = File.OpenWrite(stdOutPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = yafray.StandardOutput.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }

            String stdErrPath = Path.Combine(workingDirectory,"yafray_stderr.txt");
            using (FileStream fs = File.Create(stdErrPath)) { }
            using (FileStream fs = File.OpenWrite(stdErrPath))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    String line = yafray.StandardError.ReadToEnd();
                    if (line != null && line.Trim().Length > 0)
                        sw.WriteLine(line);
                }
            }

            Console.WriteLine("Render finished!");

            // Find the rendered image output
            string outputFile = FindOutputFile(workingDirectory);

            // Crop the output file
            // Cropped Bitmap data will be stored in the object and sent back.
            if (File.Exists(outputFile)) // salida is the default output filename (exit in spanish)
            {
                Console.WriteLine("Cropping image...");
                Renderer.TGAReader tgaReader = new Renderer.TGAReader();
                FileStream fullImage = File.OpenRead(outputFile);
                tgaReader.unpackImage(fullImage);
                Image im = tgaReader.getImage();
                imageSegment = new Bitmap(segmentWidth, segmentHeight);
                Graphics g = Graphics.FromImage(imageSegment);

                Rectangle sourceRectangle = new Rectangle(x, y, segmentWidth, segmentHeight);
                Rectangle destRectangle = new Rectangle(0, 0, segmentWidth, segmentHeight);
                g.DrawImage(im, destRectangle, sourceRectangle, GraphicsUnit.Pixel);

                // Write out image just for debug purposes.
                imageSegment.Save(Path.Combine(workingDirectory,(row + "_" + column + ".png")), ImageFormat.Png);
                fullImage.Close();
                // After cropping we can delete the original to save space.
                File.Delete(outputFile);
            }

            Console.WriteLine("All done!");
		}

        // Copy any of the shared scene files because even though we can access the
        private void CopySceneFiles(string dir)
        {
            FileInfo[] infos = Directory.GetParent(dir).GetFiles();
            foreach (FileInfo info in infos)
            {
                if (/*!info.Name.Equals(inputFile) &&*/ !info.Extension.Equals(".dll"))
                    File.Copy(info.FullName, Path.Combine(dir, info.Name));
            }
        }

        // So that we don't have to modify the outfile parameter in the users yafray xml file.
        private string FindOutputFile(string dir)
        {
            string outputFile = null;
            string[] files = Directory.GetFiles(dir, "*.tga");
            if (files != null && files.Length > 0)
                outputFile = files[0];
            return outputFile;
        }
    }
}
